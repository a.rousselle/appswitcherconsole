﻿using System.Diagnostics;
using System.Runtime.InteropServices;


[DllImport("user32.dll")]
static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

const int HIDE = 0;
const int SHOWMAXIMIZED = 3;

var windowHandles = GetWindowHandle();

if (windowHandles.Length == 0)
    return;

var interval = GetInterval();

TryClear();

foreach (var handle in windowHandles)
{
    ShowWindow(handle, HIDE);
}

Console.WriteLine("running...");

while (true)
{
    for (var i = 0; i < windowHandles.Length + 1; i++)
    {
        if (i < windowHandles.Length)
        {
            ShowWindow(windowHandles[i], SHOWMAXIMIZED);
            Thread.Sleep(interval * 1000);
            ShowWindow(windowHandles[i], HIDE);
        }
        else
            Thread.Sleep(interval * 1000);
    }
}

static IntPtr[] GetWindowHandle()
{
    var processes = Process
        .GetProcesses()
        .Where(n =>
        {
            try
            {
                var _ = n.MainModule?.FileName;
            }
            catch
            {
                return false;
            }
            return true;
        })
        .OrderByDescending(n => n.StartTime)
        .ToList();

    const int pageLength = 10;
    var maxPage = (int)Math.Ceiling(processes.Count / (decimal)pageLength);
    var pageIndex = 0;
    var res = new List<IntPtr>();

    while (true)
    {
        var displayedProcesses = processes
            .Skip(pageIndex * pageLength)
            .Take(pageLength)
            .ToList();

        TryClear();

        for (var i = 0; i < displayedProcesses.Count; i++)
        {
            var title = $"{displayedProcesses[i].MainWindowTitle} ({displayedProcesses[i].ProcessName})";
            Console.WriteLine(i + " = " + title);
        }

        Console.ForegroundColor = ConsoleColor.DarkGray;
        Console.WriteLine("n = next");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("v = validate");
        Console.ResetColor();
        Console.WriteLine("");
        Console.WriteLine($"Page: {pageIndex + 1}/{maxPage}");
        Console.WriteLine($"Processes: {res.Count}");
        Console.WriteLine("");
        Console.WriteLine("Select an index:");

        var input = Console.ReadLine();

        if (input == "n")
            pageIndex = (pageIndex + 1) % maxPage;
        else if (input == "v")
            return res.ToArray();
        else if (int.TryParse(input, out int inputIndex)
            && inputIndex >= 0
            && inputIndex < displayedProcesses.Count
            && !res.Contains(displayedProcesses[inputIndex].MainWindowHandle))
            res.Add(displayedProcesses[inputIndex].MainWindowHandle);
    }
}

static int GetInterval()
{
    while (true)
    {
        TryClear();

        Console.WriteLine("Interval (in seconds)?");

        var input = Console.ReadLine();

        if (int.TryParse(input, out int interval) && interval > 0)
            return interval;
    }
}

static void TryClear()
{
    try
    {
        Console.Clear();
    }
    catch { }
}